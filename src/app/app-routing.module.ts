import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArbitreComponent } from './component/arbitre/arbitre.component';
import { SpectateurComponent } from './component/spectateur/spectateur.component';
import { MatchComponent } from './component/match/match.component';
import { JoueurComponent } from './component/joueur/joueur.component';
import { FormArbComponent } from './component/form-arb/form-arb.component';
import { FormJeuComponent } from './component/form-jeu/form-jeu.component';
import { FormJorComponent } from './component/form-jor/form-jor.component';
import { FormSpecComponent } from './component/form-spec/form-spec.component';




const routes: Routes = [

  {path: 'arbitres', component: ArbitreComponent},
  {path: 'spectateurs', component: SpectateurComponent},
  {path: 'matchs', component: MatchComponent},
  {path: 'joueurs', component: JoueurComponent},
  {path: 'ajouterArb', component: FormArbComponent},
  {path: 'ajoutermat', component: FormJeuComponent},
  {path: 'ajouterjor', component: FormJorComponent}, 
  {path: 'ajouterspec', component: FormSpecComponent},



  



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
