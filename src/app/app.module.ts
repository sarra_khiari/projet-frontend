import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ArbitreComponent } from './component/arbitre/arbitre.component';
import { MatchComponent } from './component/match/match.component';
import { SpectateurComponent } from './component/spectateur/spectateur.component';
import { JoueurComponent } from './component/joueur/joueur.component';
import { FormArbComponent } from './component/form-arb/form-arb.component';
import { FormJorComponent } from './component/form-jor/form-jor.component';
import { FormJeuComponent } from './component/form-jeu/form-jeu.component';
import { FormSpecComponent } from './component/form-spec/form-spec.component';




@NgModule ({
  declarations: [
    AppComponent,
    ArbitreComponent,
    MatchComponent,
    SpectateurComponent,
    JoueurComponent,
    FormArbComponent,
    FormJorComponent,
    FormJeuComponent,
    FormSpecComponent,
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
