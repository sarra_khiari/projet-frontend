import { Component, OnInit } from '@angular/core';
import { Joueur } from 'src/app/model/joueur';
import { HttpClient } from '@angular/common/http';
import { CatalogeService } from 'src/app/services/cataloge.service';

@Component({
  selector: 'app-joueur',
  templateUrl: './joueur.component.html',
  styleUrls: ['./joueur.component.scss']
})
export class JoueurComponent implements OnInit {
  public joueurs:any;
  constructor(private  catService:CatalogeService) { }

  ngOnInit(): void {
  }

  onGetJoueurs(){
    this.catService.getJoueurs()
    .subscribe(data=>{this.joueurs=data;}
      ,err=>{console.log(err);

    })
  }
  onDeleteJoueur(jor){
    let conf=confirm("etes vous sur?");
    if (conf) {
      this.catService.deleteResource(jor._links.self.href)
      .subscribe(data=>{
        this.onGetJoueurs();
    
      },err=>{console.log(err);}
      )
    }
  
}
}
