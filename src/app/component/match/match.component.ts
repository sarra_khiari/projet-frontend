import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CatalogeService } from 'src/app/services/cataloge.service';
@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {
  public jeus:any;
  constructor(private  catService:CatalogeService) { }

  ngOnInit(): void {
  }

  onGetMatchs(){
    this.catService.getMatchs()
    .subscribe(data=>{this.jeus=data;}
      ,err=>{console.log(err);

    })
  }
  onDeleteMatch(mat){
    let conf=confirm("etes vous sur?");
    if (conf) {
      this.catService.deleteResource(mat._links.self.href)
      .subscribe(data=>{
        this.onGetMatchs();
    
      },err=>{console.log(err);}
      )
    }
}
}
