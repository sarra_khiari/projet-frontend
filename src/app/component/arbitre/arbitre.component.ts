import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CatalogeService } from 'src/app/services/cataloge.service';
@Component({
  selector: 'app-arbitre',
  templateUrl: './arbitre.component.html',
  styleUrls: ['./arbitre.component.scss']
})
export class ArbitreComponent implements OnInit {
  public arbitres:any;
  constructor(private  catService:CatalogeService) { }

  ngOnInit(): void {
  }

  onGetArbitres(){
    this.catService.getArbitres()
    .subscribe(data=>{this.arbitres=data;}
      ,err=>{console.log(err);

    })
  }
  onDeleteArbitre(arb){
    let conf=confirm("etes vous sur?");
    if (conf) {
      this.catService.deleteResource(arb._links.self.href)
      .subscribe(data=>{
        this.onGetArbitres();
    
      },err=>{console.log(err);}
      )
    }
   
  }

}
