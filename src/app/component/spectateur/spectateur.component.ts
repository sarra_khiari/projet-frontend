import { Component, OnInit } from '@angular/core';
import { Spectateur } from 'src/app/model/spectateur';

import { HttpClient } from '@angular/common/http';
import { CatalogeService } from 'src/app/services/cataloge.service';
@Component({
  selector: 'app-spectateur',
  templateUrl: './spectateur.component.html',
  styleUrls: ['./spectateur.component.scss']
})
export class SpectateurComponent implements OnInit {
  public spectateurs:any;
  constructor(private  catService:CatalogeService) { }

  ngOnInit(): void {
  }

  onGetSpectateurs(){
    this.catService.getSpectateurs()
    .subscribe(data=>{this.spectateurs=data;}
      ,err=>{console.log(err);

    })
  }
  onDeleteSpectateur(spec){
    let conf=confirm("etes vous sur?");
    if (conf) {
      this.catService.deleteResource(spec._links.self.href)
      .subscribe(data=>{
        this.onGetSpectateurs();
    
      },err=>{console.log(err);}
      )
    }
}
}
