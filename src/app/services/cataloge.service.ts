import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Arbitre } from '../model/arbitre';
import { Joueur } from '../model/joueur';
import { Match } from '../model/match';
import { Spectateur } from '../model/spectateur';

@Injectable({
  providedIn: 'root'
})
export class CatalogeService {

  public host:string="http://localhost:8083";

  constructor(private httpClient:HttpClient) { }

  
  public getArbitres(){
    return this.httpClient.get(this.host+"/arbitres")
  }
  public getJoueurs(){
    return this.httpClient.get(this.host+"/joueurs")
  }
  public getMatchs(){
    return this.httpClient.get(this.host+"/jeus")
  }
  public getSpectateurs(){
    return this.httpClient.get(this.host+"/spectateurs")
  }

  public deleteResource(url){
    return this.httpClient.delete(url);
  }
  public updateResource(url,data){
    return this.httpClient.put(url,data);
  }
 
 
}
